/* 1. Опишіть своїми словами що таке Document Object Model (DOM)

Об'єктна модель документа (DOM) – це програмний інтерфейс (API) для HTML і XML документів. 

DOM надає структуроване подання документа і визначає те, як ця структура може бути доступна з програм, 
які можуть змінювати вміст, стиль та структуру документа. 
Подання DOM складається із структурованої групи вузлів та об'єктів, які мають властивості та методи. 

Фактично, DOM з'єднує веб-сторінку з мовами опису сценаріїв чи мовами програмування.


2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

На відміну від innerText, однак, innerHTML дозволяє працювати з форматованим текстом HTML і не виконує автоматичного кодування та декодування тексту. Іншими словами, 
innerText витягує та встановлює вміст тега у вигляді простого тексту, тоді як innerHTML витягує та встановлює вміст у форматі HTML.


3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Вибір елементів здебільшого виконується за допомогою цих методів:

     querySelector;
     querySelectorAll
	 
	 
Крім них є ще:

     getElementById;
     getElementsByClassName;
     getElementsByTagName;
     getElementsByName.
	 
Кожен має свої недоліки та переваги.

*/
const listOfTags = document.getElementsByTagName('p');

for (let tag of listOfTags) {
	
  tag.style.backgroundColor = '#ff0000';
}
const optionListEl = document.getElementById('optionsList');

console.log(optionListEl);

console.log(optionListEl.parentElement);

optionListEl.childNodes.forEach((e) => console.log(e.nodeType));

optionListEl.childNodes.forEach((e) => console.log(e.nodeName));

document.getElementById('testParagraph').textContent = 'This is a paragraph';

const listMainHeader = document.querySelector('.main-header').children;
console.log(listMainHeader);

for (let i = 0; i < listMainHeader.length; i++) {
  listMainHeader[i].classList.add('nav-item');
}
const listSectionTitle = document.querySelectorAll('.section-title');

for (let i = 0; i < listSectionTitle.length; i++) {
	
  listSectionTitle[i].classList.remove('section-title');
}
